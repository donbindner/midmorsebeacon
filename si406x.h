/* Copyright 2015 by Donald J Bindner <don.bindner@gmail.com>            */
/* Copyright 2013 Philip Heron <phil@sanslogic.co.uk>                    */
/*                                                                       */
/* Adapted from code of Stefan Biereigel.                                */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef _SI406X_H
#define _SI406X_H

#define SY_SEL_1	(0x01 << 3)

#define RF_FREQ_HZ	(146565000.0f) 
#define XO_FREQ		30000000UL
#define XO_CAP		0x2D
#define OUTPUT_PWR	0x7F	/* 0x10~10mW, 0x34~50mW, and 0x7F~85mW */
#define RF_DEV_HZ	1300.0f

#define OUTDIV_2M	24
#define F_INT_2M	(2 * XO_FREQ / 24 )

#define FDIV_INTE	((RF_FREQ_HZ / F_INT_2M) - 1)
#define FDIV_FRAC	((RF_FREQ_HZ - F_INT_2M * (int)FDIV_INTE)*(1UL << 19)) / F_INT_2M

#define FDEV		(((1UL << 19) * OUTDIV_2M * RF_DEV_HZ)/(2*XO_FREQ))

#define FVCO_DIV_24		0x05
#define MODEM_FREQ_DEV		0x0a
#define MODEM_FREQ_OFFSET	0x0d

/* property values */

#define MOD_TYPE_2FSK		0x02
#define MOD_TYPE_2GFSK		0x03

#define MOD_SOURCE_DIRECT	(0x01 << 3)
#define MOD_DIRECT_MODE_SYNC	(0x00 << 7)
#define MOD_DIRECT_MODE_ASYNC	(0x01 << 7)

#define MOD_GPIO_0		(0x00 << 5)
#define MOD_GPIO_1		(0x01 << 5)
#define MOD_GPIO_2		(0x02 << 5)
#define MOD_GPIO_3		(0x03 << 5)

#ifdef __cplusplus
extern "C" {
#endif

extern void si_init(void);
extern void si_radio_on(void);
extern void si_radio_off(void);
extern void si_set_frequency();
extern void si_set_channel(unsigned char channel);
extern void si_set_offset(int offset);
extern int si_get_temperature();

#ifdef __cplusplus
}
#endif

#endif

