/* main.c
 * Main program.  Wake up every 5 minutes and transmit a Morse
 * code message. */

/* Copyright 2015 by Donald J Bindner <don.bindner@gmail.com>            */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <msp430.h>
#include "si406x.h"
#include "morse.h"
#include "delay.h"
#include "ticks.h"

volatile long transmissionCounter = INITIALTICKS;	/* delayed start time */
volatile int leapCounter = LEAPDENOM;		/* number of transmissions before we add 'leap ticks' */

void powerOnRadio() {
    si_init();
}

int main(void) {
    WDTCTL = WDTPW + WDTHOLD;

    setMainClockTo1MHz();
    initializePWMtimer();

    powerOnRadio();

    __bic_SR_register( GIE );		/* no interrupts for a bit */

    /* Set the watchdog timer to use external drive and
     * generate interrupts for precise timing and delays */
    BCSCTL3 = LFXT1S_3 | XCAP_0;	/* Digital input no capacitance */
    WDTCTL = WDT_ADLY_1000;		/* 1MHz/32768 ~ 30.5176 clk/second */
    IFG1 &= ~WDTIFG;			/* clear interrupt flag */
    IE1 |= WDTIE;			/* enable watchdog interrupts */

    __bis_SR_register( LPM3_bits + GIE );	/* enable interrupts and sleep */

    /* the main transmit loop */
    while( 1 ) {
	turnSilenceOn();
	enablePWMpin();

	/* queue up message and then sleep until sent */
	si_transmitter_on();
	startMorseMessage();
	__bis_SR_register( LPM0_bits + GIE );
	si_transmitter_off();
	
	disablePWMpin();

	/* sleep in deep sleep until next transmission */
	__bis_SR_register( LPM3_bits + GIE );
    }
}

/* Transmission window countdown timer. */
void WDT_ISR(void) __attribute__((interrupt(WDT_VECTOR)));
void WDT_ISR(void)
{
    if( transmissionCounter == 0 ) {
	transmissionCounter = WINDOWTICKS;	/* ticks until transmission restarts */

	/* To improve accuracy we sometimes add 'leap ticks' */
	leapCounter--;
	if( leapCounter == 0 ) {
	    transmissionCounter += LEAPNUMER;
	    leapCounter = LEAPDENOM;
	}

	__bic_SR_register_on_exit( LPM3_bits );		/* wake up the main program */
    } 

    transmissionCounter--;

    /* clear the interrupt flag for watchdog timer */
    IFG1 &= ~WDTIFG;
}
