#ifndef _MORSE_H
#define _MORSE_H

void setMainClockTo1MHz();

void turnToneOn();
void turnSilenceOn();

void enablePWMpin();
void disablePWMpin();

void startMorseMessage();

#endif
