MCU=msp430g2210
CC=msp430-gcc
MSPDEBUG=mspdebug
FLAGS=-O2

main.elf: main.o morse.o si406x.o delay.o
	$(CC) -mmcu=$(MCU) $(FLAGS) -o $@ $^
	msp430-size $@

main.o: main.c morse.h si406x.h delay.h ticks.h
	$(CC) -mmcu=$(MCU) $(FLAGS) -c -o $@ $<

si406x.o: si406x.c si406x.h
	$(CC) -mmcu=$(MCU) $(FLAGS) -c -o $@ $<

morse.o: morse.c morse.h message.h
	$(CC) -mmcu=$(MCU) $(FLAGS) -c -o $@ $<

ticks.h: message.h
	touch ticks.h

# It's necessary to edit setup.py to insert your own MO-* message and call sign.
# This rule is also responsible for setting up ticks.h.
message.h: setup.py
	@if grep -q N0CALL setup.py ; then /bin/echo -e "\nERROR: You must edit setup.py with your call sign!\n"; false; fi
	python $< > $@

delay.o: delay.s delay.h
	msp430-as -mmcu=$(MCU) -o delay.o delay.s

install: main.elf
	$(MSPDEBUG) rf2500 "prog main.elf"

clean:
	rm -f *.bak *.o message.h ticks.h main.elf
