/* Adapted to MSP430 by Donald J Bindner <don.bindner@gmail.com>         */
/* Ported to C by Philip Heron <phil@sanslogic.co.uk>                    */
/* Code modified by Arko for the Si406x                                  */
/* Code based on KT5TK's Si446x                                          */
/* pecanpico2 Si446x Driver copyright (C) 2013  KT5TK                    */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <msp430.h>
#include "si406x.h"
#include "delay.h"

/* Shutdown pin is P1.0, and Slave Select is P1.1 */
#define SDN BIT0
#define SS BIT1

/* SDI pin is P1.2, SDO is P1.3, and SCK is P1.4 */
#define SDI BIT2 // MOSI
#define SDO BIT3 // MISO
#define SCK BIT4

/* States */
#define STATE_READY   0x03
#define STATE_TX_TUNE 0x05
#define STATE_TX      0x07

static unsigned char _spi_transfer(unsigned char v)
{
  int i;

  for( i=0; i<8; i++ ) {
    if( v & 0x80 ) {	/* Send out most significant bit */
      P1OUT |= SDI;
    } else {
      P1OUT &= ~SDI;
    }

    v <<= 1;		/* Shift to make room for new least significant bit */

    P1OUT |= SCK;	/* Pulse clock high */

    if( P1IN & SDO ) {	/* Get least significant bit */
      v |= 0x01;
    } else {
      v &= ~0x01;
    }

    P1OUT &= ~SCK;	/* Clock low */
  }
    
  return(v);
}

inline void unselect_slave() {
  /* Set slave select pin high (inactive). */
  P1OUT |= SS;
}

inline void select_slave() {
  /* Set slave select pin low (active). */
  P1OUT &= ~SS;
}


static void _wait_for_cts()
{
  unsigned char r;
  unsigned int timeout=0x0FFF;

  /* Poll CTS over SPI method */
  do {
    select_slave();

    _spi_transfer(0x44);
    r = _spi_transfer(0x00);

    unselect_slave();
  } while((r != 0xFF) && (timeout--));
}

static void _send_command(unsigned char *data, unsigned char length)
{
  _wait_for_cts();

  select_slave();

  /* Send the command and data */
  for(; length; length--)
    _spi_transfer(*(data++));

  unselect_slave();
}

static void _power_up()
{
  unsigned char data[] = {
    0x02, /* power up command */
    0x01, /* No patch */
    0x01, /* use external TXCO */
    (XO_FREQ >> 24) & 0xFF, /* TXCO frequency */
    (XO_FREQ >> 16) & 0xFF,
    (XO_FREQ >> 8) & 0xFF,
    XO_FREQ & 0xFF
  };

  _send_command(data, sizeof(data));
}

static void _set_modem()
{
  /* Set to 2FSK mode, driven from GPIO0 pin */
  unsigned char data[] = { 
    0x11,	/* set property command */
    0x20,	/* modem properties */
    0x01,	/* 1 byte of data */
    0x00,	/* starting location */
    MOD_TYPE_2GFSK | MOD_SOURCE_DIRECT | MOD_GPIO_0 | MOD_DIRECT_MODE_SYNC
  };

  _send_command(data, sizeof(data));
}

static void _set_clock()
{
  unsigned char data[] = {
    0x11,	/* set property command */
    0x00,	/* group */
    0x02,	/* 2 bytes of data */
    0x00, 	/* start ID */
    XO_CAP,	/* 2-11pF capacitance as hex 0x00-0x7F */
    0x70	/* 0x00 for 32kHz clock disabled (use 0x01 to enable RC osc
                   or 0x70 for main clock divided by 30) */
  };

  _send_command(data, sizeof(data));
}

static void _set_output_pwr() {
  unsigned char data[] = {
    0x11,	/* set property command */
    0x22,	/* group */
    0x04,	/* 4 bytes of data */
    0x00,	/* start ID */
    0x08,	/* Power amplifier mode - Class E/Square W */
    OUTPUT_PWR,	/*** OUTPUT POWER - choose from 0x00 to 0x7F ***/
    0x00,	/* PA Bias and duty cycle of the TX clock source */
    0x1D,	/* PA ramping parameters */
  };

  _send_command( data, sizeof(data));
}

static void _set_gpio_for_tx() {
  unsigned char data[] = { 
    0x13,	/* set GPIO pin configuration */
    0x13,	/* GPIO0 = TX_DATA */
    0x01,	/* GPIO1 TRISTATE (i/o drivers disconnected) */
    0x01,	/* GPIO2 TRISTATE (i/o drivers disconnected) */
    0x07,	/* 0x01 for GPIO3 TRISTATE (use 0x05 for 32kHz clock ) 
                   or 0x07 for divided main clock */
    0x00,	/* nIRQ unchanged */
    0x00,	/* SDO unchanged */
    0x00	/* SDI unchanged */
  };

  _send_command(data, sizeof(data));
}

static void _set_state(unsigned char state)
{
  unsigned char data[] = { 
    0x34,	/* change state command */
    state
  };

  _send_command(data,sizeof(data));
}

inline void shutdown_radio() {
  /* Set shutdown pin high. */
  P1OUT |= SDN;
}

inline void wakeup_radio() {
  /* Set shutdown pin low. */
  P1OUT &= ~SDN;
}

void si_init(void)
{
  /* Set the SDN pin for output */
  shutdown_radio();
  P1DIR |= SDN;

  /* Configure SPI pins for output */
  P1OUT &= ~(SCK+SDI);		     /* Clock and SDI/MOSI starts low */
  unselect_slave();                             /* SS high */
  P1DIR |= SS + SDI + SCK;

  /* Configure SDO pin for input, enable internal pull-up */
  P1DIR &= ~SDO;
  P1OUT |= SDO;
  P1REN |= SDO;

  /* "The SDN pin needs to be held high for
   	 *  at least 10us before driving low again
   	 *  so that internal capacitors can discharge" */
  delay10us(2);

  /* Wake up the radio from shutdown. */
  wakeup_radio();

  /* "High time for VDD to fully settle POR circuit." (Min: 10ms) */
  delay10us(2000);

  /* Send the POWER_UP command */
  _power_up();

  /* Set the initial frequency and power */
  _set_clock();
  si_set_frequency();
  _set_output_pwr();

  /* Set to FSK mode driven by GPIO0 */
  _set_modem();
  _set_gpio_for_tx();

  /* Set the "TX Tune" state */
  _set_state(STATE_TX_TUNE);
}

void si_transmitter_on(void)
{
  _set_state(STATE_TX);
}

void si_transmitter_off(void)
{
  _set_state(STATE_READY);
}

void _set_property_8( unsigned char group, unsigned char prop, unsigned char val ) {
  unsigned char data[] = {
    0x11,	/* set property command */
    group,
    0x01,	/* 1 byte of data */
    prop,	/* inital address */
    val&0xFF	/* data */
  };

  _send_command(data, sizeof(data));
}

void _set_property_16( unsigned char group, unsigned char prop, unsigned val ) {
  unsigned char data[] = {
    0x11,	/* set property command */
    group,
    0x02,	/* 2 bytes of data */
    prop,	/* initial address */
    val>>8,	/* data */
    val&0xFF	/* data */
  };

  _send_command(data, sizeof(data));
}

void _set_property_24( unsigned char group, unsigned char prop, unsigned long val ) {
  unsigned char data[] = {
    0x11,	/* set property command */
    group,
    0x03,	/* 3 bytes of data */
    prop,	/* initial address */
    (val>>16)&0xFF,	/* data */
    (val>>8)&0xFF,	/* data */
    val&0xFF		/* data */
  };

  _send_command(data, sizeof(data));
}

void si_set_frequency( void )
{
  /* setup divider to 24 (for 2m amateur radio band) */
  _set_property_8(
      0x20,	/* modem property */
      0x51,	/* clkgen band */
      SY_SEL_1 | FVCO_DIV_24);

  /* set up the integer divider */
  _set_property_8(
      0x40,	/* frequency control property */
      0x00,	/* FREQ_CONTROL_INTE */
      (unsigned char)(FDIV_INTE));

  /* set up the fractional divider */
  _set_property_24(
      0x40,	/* frequency control property */
      0x01,	/* FREQ_CONTROL_FRAC */
      (unsigned long)(FDIV_FRAC));

  /* setup frequency deviation offset */
  _set_property_16(
      0x20,	/* modem property */
      MODEM_FREQ_OFFSET,
      0);

  /* setup frequency deviation */
  _set_property_24(
      0x20,	/* modem property */
      MODEM_FREQ_DEV,
      (unsigned)(2*FDEV));
}
