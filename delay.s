;    Copyright (C) 2014  Don Bindner
;     adapted from code by Kevin Timmerman
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

nop2:       .macro                      ; 2 cycle, 1 word NOP
            jmp     $ + 2               ;
            .endm                       ;

            .global delay10us           ; void delay10us(unsigned)

; Delay in units of 10 cycles (10 us at 1 MHz)
delay10us:                              ; Call to us used 5 cycles
            cmp     #2, R15             ; Compare to minimum possible - 1 cycles
            jlo     dlyret              ; Below minimum, return... (unsigned <) - 2 cycles
            dec     R15                 ; Adjust loop count for overhead - 1 cycles
            jmp     eloop               ; Make the first iteration shorter - 2 cycles

dloop:      nop2                        ; 2 cycles
            nop2                        ; 2 cycles
eloop:      nop2                        ; 2 cycles
            nop                         ; 1 cycle
            dec     R15                 ; Decrement loop count - 1 cycle
            jne     dloop               ; Loop if not zero - 2 cycles
                                        ;
dlyret:     ret                         ; Return to caller - 3 cycles
                                        ;
            .end                        ;
